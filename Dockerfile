FROM python:3
ENV PYTHONUNBUFFERED=1
RUN apt update
RUN apt install gettext libgettextpo-dev -y
RUN mkdir /code
WORKDIR /code
COPY requirements.txt /code/
RUN pip install -r requirements.txt
COPY . /code/