# Api.chucknorris.io on Django+Bulma+Docker

I created an application with django to make searchs and request data from api.chucknorris.io. 
This app is multilanguage and I made some tests to check some parts of this app.

## Requirements
- Docker >= 19.03.13

## How to deploy this test
- Execute docker-compose build
- Execute docker-compose run web python3 manage.py migrate 
  ( Execute db migrations required for django to work and apply app models migrations )
- Execute docker-compose up ( To start all other required services and launch server on localhost )
- Access to this website by typing http://localhost:8000 on your browser

## Tests

To execute django tests created on tests.py run this command

- docker-compose run web python3 manage.py test

## Backend

To access to django backend to check store data first you'll need a to create a super admin user with this command

- docker-compose run web python3 manage.py createsuperuser

After that you can access to backen by inserting this url on your browser http://localhost:8000/admin

NOTE: Don't worry if you don't use /en|/es locales by default django will redirect you to your browser language and if you
change the app language will be stored on your session.

