from django.contrib import admin
from .models import Search, Result

class SearchAdmin(admin.ModelAdmin):
    list_display = ('search_type', 'search', 'category', 'email')

class ResultAdmin(admin.ModelAdmin):
    list_display = ('search', 'data')

admin.site.register(Search, SearchAdmin)
admin.site.register(Result, ResultAdmin)

