from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from .models import Search, ApiChuckNorris

class SearchForm(forms.ModelForm):
    search_type = forms.TypedChoiceField(label=_('Search Type'), initial=Search.TYPE_WORDS ,choices=Search.SEARCH_TYPE_CHOICES, widget=forms.RadioSelect)

    def clean(self):
        super().clean()
        search_type = self.cleaned_data.get('search_type')
        search = self.cleaned_data.get('search')
        category = self.cleaned_data.get('category')

        # Validate category requirement
        if search_type == Search.TYPE_CATEGORY and category is None:
            self.add_error('category', ValidationError('You must select one category'))
        elif search_type == Search.TYPE_WORDS and search is None:
            self.add_error('search', ValidationError('You must enter a text to search by words'))
    
        return self.cleaned_data
            

    class Meta:
        fields = ('search_type', 'category', 'search', 'email')
        model = Search