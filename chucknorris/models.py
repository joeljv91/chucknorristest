from django.db import models
from django.shortcuts import reverse
from django.utils.translation import ugettext_lazy as _

import requests, json, urllib

class ApiChuckNorris:
    ENDPOINT = "https://api.chucknorris.io/jokes"
    ENDPOINT_RANDOM = ENDPOINT + "/random"
    ENDPOINT_CATEGORIES = ENDPOINT + "/categories"
    ENDPOINT_SEARCH = ENDPOINT + "/search"

    def get_categories():

        response = requests.get(ApiChuckNorris.ENDPOINT_CATEGORIES)
        if response.status_code != 200:
            raise Exception('ApiChuck is not responding')
        
        categories = []
        for category in json.loads(response.text):
            categories.append((category, category.capitalize()))

        return categories

    def search(text):

        args = {'query': text}
        response = requests.get(ApiChuckNorris.ENDPOINT_SEARCH+'?'+urllib.parse.urlencode(args))
        if response.status_code != 200:
            raise Exception('ApiChuck is not responding')

        return json.loads(response.text).get('result')

    def random(category = None):
        args = {}
        if category is not None:
            args['category'] = category

        if len(args) > 0:
            response = requests.get(ApiChuckNorris.ENDPOINT_RANDOM+'?'+urllib.parse.urlencode(args))
        else:
            response = requests.get(ApiChuckNorris.ENDPOINT_RANDOM)

        if response.status_code != 200:
            raise Exception('ApiChuck is not responding')
        
        return json.loads(response.text).get('value')
        
def get_category_choices():
    return [('', '--')]+ApiChuckNorris.get_categories()

class Search(models.Model):
    TYPE_WORDS = 'words'
    TYPE_CATEGORY = 'category'
    TYPE_RANDOM = 'random'

    SEARCH_TYPE_CHOICES = (
        (TYPE_WORDS, _('Words')),
        (TYPE_CATEGORY, _('Category')),
        (TYPE_RANDOM, _('Random'))
    )

    search_type = models.CharField(_('Search Type'), choices=SEARCH_TYPE_CHOICES, max_length=8, default=TYPE_WORDS)
    category = models.CharField(_('Category'), max_length=100, null=True, blank=True, choices=get_category_choices())
    search = models.CharField(_('Search Query'), max_length=200, null=True, blank=True)
    email = models.EmailField(_('Email'), blank=True, null=True)

    @staticmethod
    def get_types():
        return (Search.TYPE_WORDS, Search.TYPE_RANDOM, Search.TYPE_CATEGORY)

class Result(models.Model):
    search = models.OneToOneField(Search, on_delete=models.CASCADE)
    data = models.JSONField()

    def is_multiresult(self):
        return self.search.search_type == Search.TYPE_WORDS

    def get_absolute_url(self):
        return reverse("result_detail", kwargs={"pk": self.pk})
    

