from django import template

register = template.Library()

@register.filter(name='add_tag_class')
def add_tag_class(tag, extra_class='label'):
    return tag.replace('<label ', '<label class="'+extra_class+'" ')

@register.filter(name='bulma_input')
def bulma_input(value, extra_class):
    """
    Add input class to field
    """

    css_classes = value.field.widget.attrs.get('class', '')
    # check if class is set or empty and split its content to list (or init list)
    if css_classes:
        css_classes = css_classes.split(' ')
    else:
        css_classes = []
    
    # Append our class
    css_classes.append(extra_class)

    return value.as_widget(attrs={'class': ' '.join(css_classes)})

@register.filter
def to_class_name(value):
    return value.__class__.__name__
