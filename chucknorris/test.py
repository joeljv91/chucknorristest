from django.test import TestCase
from chucknorris.models import ApiChuckNorris, Search, Result
from chucknorris.forms import SearchForm
from django.test import Client

import unittest

# TEST MODELS
class ModelTestsCase(TestCase):
    """
    Test correct model creation with valid data and correct multiresult method
    """
    SEARCH_TEXT="batman sucks 3459321"

    def setUp(self):
        search = Search.objects.create(search_type=Search.TYPE_WORDS, search=self.SEARCH_TEXT)
        result = Result.objects.create(search=search, data=[])

    def testing_models(self):
        search = Search.objects.get(search=self.SEARCH_TEXT)
        self.assertTrue(search.result.is_multiresult())
        

class ApiTestsCase(TestCase):        

    def test_api_categories(self):
        self.assertTrue(len(ApiChuckNorris.get_categories()) > 0)

    def test_api_words_search(self):
        self.assertTrue(len(ApiChuckNorris.search('batman')) > 0)

# TEST FORMS
class FormTestsCase(TestCase):

    def test_invalid_search_type(self):
        """
        Test to force search type not valid, we except this to fail because it's a forced validation
        """
        form = SearchForm(data={'search_type': 'dsfds', 'search': 'batman'})
        self.assertFalse(form.is_valid())

    def test_invalid_category_search(self):
        """
        Force search category without a category invalid form
        """
        form = SearchForm(data={'search_type': Search.TYPE_CATEGORY})
        self.assertFalse(form.is_valid())

    def test_invalid_category_search(self):
        """
        Force search words without a query invalid form
        """
        form = SearchForm(data={'search_type': Search.TYPE_WORDS, 'search': ''})
        self.assertFalse(form.is_valid())
    

# TEST VIEWS
class ViewsTestsCase(TestCase):

    def setUp(self):
        self.client = Client()

    def testing_search_post(self):
        """
        Rest post to search view redirect to result page
        """
        response = self.client.post('/en/', {'search_type': Search.TYPE_WORDS, 'search': 'batman'})
        self.assertEqual(response.status_code, 302)

    def test_403_search(self):
        """
        Test empty post
        """
        response = self.client.post('/en/', {})
        self.assertEqual(response.status_code, 403)

    def test_200_get_search(self):
        """
        Test first access to search url
        """
        response = self.client.get('/en/')
        self.assertEqual(response.status_code, 200)



