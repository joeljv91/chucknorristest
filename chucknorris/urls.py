from django.contrib import admin
from django.urls import path
from django.conf.urls.i18n import i18n_patterns

from . import views


urlpatterns = i18n_patterns(
    path('admin/', admin.site.urls),
    path('result/<str:pk>', views.ResultView.as_view(), name='result_detail'),
    path('change-language', views.change_language, name='change_language'),
    path('', views.search, name="search_page")
)
