from django.shortcuts import render, redirect
from .forms import SearchForm
from .models import Search, ApiChuckNorris, Result
from django.http import HttpResponseForbidden
from django.views.generic.detail import DetailView
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.translation import gettext as _
from django.http import HttpResponseRedirect
from django.conf import settings

def search(request):
    """
    Search view responsible to show search form and store search and result data. Also if user insert an email, an email with
    results will be sent to the user
    """
    if request.method == 'POST':

        search_type = request.POST.get('search_type')
        search = request.POST.get('search')
        category = request.POST.get('category')

        if search_type not in Search.get_types():
            # Raise 403 not allowed if type is not one of our types
            return HttpResponseForbidden('Search type not allowed')

        form = SearchForm(request.POST)
        if form.is_valid():

            # Get result from api
            if search_type == Search.TYPE_WORDS:
                result = ApiChuckNorris.search(search)
            elif search_type == Search.TYPE_CATEGORY:
                result = ApiChuckNorris.random(category)
            elif search_type == Search.TYPE_RANDOM:
                result = ApiChuckNorris.random()

            # Save Search
            search = form.save()

            # Save search and result
            result = Result.objects.create(search=search, data=result)

            # If search has email sent results to mail
            send_mail(
                _('Your ApiChuck Search Results'),
                render_to_string('mail/result.html', {'result': result}), 
                'apichuck@apichuck.com',
                [search.email]
            )

            # Redirect to result page
            return redirect(result.get_absolute_url())

        return render(request, 'search.html', {"form": form })
    # If it's not post return form
    form = SearchForm()

    return render(request, 'search.html', {"form": form })

class ResultView(DetailView):
    template_name = 'result.html'
    model = Result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        page = self.request.GET.get('page')

        result = self.get_object()
        paginator = Paginator(result.data, 10)
        try:
            data = paginator.page(page)
        except PageNotAnInteger:
            data = paginator.page(1)
        except EmptyPage:
            data = paginator.page(paginator.num_pages)

        context["data"] = data

        return context

def change_language(request):
    response = HttpResponseRedirect('/')
    if request.method == 'POST':
        language = request.POST.get('language')
        if language:
            if language != settings.LANGUAGE_CODE and [lang for lang in settings.LANGUAGES if lang[0] == language]:
                redirect_path = f'/{language}/'
            elif language == settings.LANGUAGE_CODE:
                redirect_path = '/'
            else:
                return response
            from django.utils import translation
            translation.activate(language)
            response = HttpResponseRedirect(redirect_path)
            response.set_cookie(settings.LANGUAGE_COOKIE_NAME, language)
            
    return response
    